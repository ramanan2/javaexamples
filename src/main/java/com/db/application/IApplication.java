package com.db.application;

public interface IApplication {
    /**
     * Returns the version of the application
     * @return
     */
    String getVersion();

    /**
     * Deposit money to bank
     * @param accountid
     * @param amount
     */
    double deposit(String accountid, double amount);

    /**
     * Withdraw money from bank
     * @param accountid
     * @param amount
     * @return
     */
    double withdraw(String accountid, double amount);
}
