package com.db.application;

public class ApplicationImpl implements IApplication{
    private double balance = 0;
    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public double deposit(String accountid, double amount) {
        return this.balance + amount;
    }

    @Override
    public double withdraw(String accountid, double amount) {
        return this.balance - amount;
    }
}
