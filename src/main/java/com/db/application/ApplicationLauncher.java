package com.db.application;

public class ApplicationLauncher {
    public static void main(String args[])
    {
        System.out.println(new ApplicationImpl() instanceof IApplication); //is-a -> always returns true

        IApplication application = new ApplicationImpl();
        System.out.println(application.deposit("0001",1001));
        System.out.println(application.withdraw("0001",993));
    }
}
